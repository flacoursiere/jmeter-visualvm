package com.lacouf.controlleur;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class MainControlleur {
    
    Logger log = LoggerFactory.getLogger(MainControlleur.class);

    @GetMapping(value = "/monurl")
    public @ResponseBody UserDTO monUrl(@RequestParam(value="option", required=false) String option) {
        log.info("params : " + option );
        
        String result = doSomething(option);
        
        return new UserDTO(result);
    }
    
    private String doSomething(String option) {
        StringBuffer result = new StringBuffer();
        result.append(option);
        result.append("\n");
        
        int sleepTime = 0;
        for (int i = 0; i < 100; i++) {
            switch (i) {
            case 10:
                sleepTime = 15;
                result.append(getMessage(sleepTime, i));
                sleep(1500);
                break;
            case 15:
                sleepTime = new Random().nextInt(100);
                result.append(getMessage(sleepTime, i));
                
            }
            sleep(10);
        }
        
        return result.toString();
    }

    private String getMessage(int sleepTime, int i) {
        return "sleeping " + sleepTime + " ms on iteration " + i +"\n";
    }

    private void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static class UserDTO {
        private String option;

        public UserDTO(String option) {
            this.option = option;
        }

        public String getOption() {
            return option;
        }
        
    }
}
