package com.lacouf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class JMeterVisualVMApplication {
    
    Logger log = LoggerFactory.getLogger(JMeterVisualVMApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(JMeterVisualVMApplication.class, args);
	}
	
	@Bean
    public CommandLineRunner demo() {
        return (args) -> {
            log.info("application démarrée");
        };
    }
}
